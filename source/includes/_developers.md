# Developers

Represents studios responsible for game development, such as Dumativa Game Studio, Cygames, and others.

## Developer object

Detailed descriptions of developers resource attributes with example payload.

```json
{
  "id": "5a3970db88102444ef00021b",
  "type": "developers",
  "attributes": {
    "name": "MercurySteam"
  }
}
```

**ATTRIBUTES**

| Name   | Description                            |
| ------ | -------------------------------------- |
| `id`   | String representing the developer id   |
| `type` | Resource type                          |
| `name` | String representing the developer name |

## GET: Retrieve Developer

`GET /v3/developers/:id`

> Get a developer.

```shell
curl -i -X GET 'https://api.nuuvem.com/v3/developers/5a3970db88102444ef00021b' \
  -H "Authorization: Bearer ${NUUVEM_API_TOKEN}" \
  -H "Accept: application/vnd.api+json" \
  -H "Accept-Language: en"
```

> The above command returns the following **response body and headers**:

```http
HTTP/2.0 200 OK
Content-Type: application/vnd.api+json; charset=utf-8
Content-Language: en

{
  "id": "5a3970db88102444ef00021b",
  "type": "developers",
  "attributes": {
    "name": "MercurySteam"
  },
  "links": {
    "self": "https://api.nuuvem.com/v3/developers/5a3970db88102444ef00021b"
  }
}
```

| Parameter | Default | Description                |
| --------- | ------- | -------------------------- |
| `id`      | none    | The requested developer ID |

**LINKS**

| Name   | Description                                        |
| ------ | -------------------------------------------------- |
| `self` | URL for the selected developer resource on the API |
