# Products

The products endpoints collection encapsulates the representation of our offerings as showcased on the Nuuvem store. Within this section, you will discover valuable resources to efficiently manage requests related to product endpoints.

## Product object

Detailed **Product** resource attributes descriptions with payload example.

```json
{
  "id": "557dbc2c69702d0a9c36d900",
  "type": "products",
  "attributes": {
    "name": "Castlevania: Lords of Shadow 2",
    "sku": 1724,
    "slug": "castlevania-lords-of-shadow-2",
    "store_url": "https://www.nuuvem.com/br-en/item/castlevania-lords-of-shadow-2",
    "purchasable": true,
    "pricing": {
      "currency": "BRL",
      "full_amount": 72.99,
      "sale_amount": 72.99,
      "discount_percentage": 0,
      "valid_from": "2017-08-25T19:32:36Z",
      "valid_until": null,
      "full_amount_formatted": "R$ 72,99",
      "sale_amount_formatted": "R$ 72,99"
    },
    "images": {
      "background": {
        "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/backgrounds/zjindmnjd11jv45umfou.jpg"
      },
      "banner": {
        "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/banners/hwuv5id0osnjsh3wmxkl.jpg"
      },
      "boxshot": {
        "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/boxshots/ebbnueufjev1w72t1h4k.jpg"
      },
      "sharing": {
        "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/sharing_images/s61wdikrzbzvgtxpxtjm.jpg"
      },
      "screenshots": [
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/hszxk81vqrawq5kyswfw.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/amztoqpxldan5jkrhezx.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/xced9eitne6fcqlwa4sp.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/vtqlrsrvmqcf8fevsf9e.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/ncj8lqejtyh8mz4ts9xt.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/z65facedrq1fpvqhv3vo.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/awvasfgbqurbqzmehmim.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/aqrcdndexnbymgkg6duk.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/vchigx7deexriaohtn1n.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/xhz9ixjuth2o8j3xuidk.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/trgmo34kvodri9x99fih.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/kbxzshagypghvtmihsut.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/pnoy4gn9fryirwygbhet.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/rsjppukgwuleo6sylmvh.jpg"
        },
        {
          "url": "https://assets.nuuvem.com/image/upload/v1/products/557dbc2a69702d0a9cc7d800/screenshots/rgcre8zo6exrqcktxawc.jpg"
        }
      ]
    },
    "about": "<p>Dracula, weak and yearning release from immortality, ...himself!</p>\r\n",
    "short_info": null,
    "important_information": null,
    "pre_sale_information": null,
    "early_play_information": null,
    "bonus_content": null,
    "highlights": null,
    "footer_notes": "<p>&copy; 2014 KONAMI Digital Entertainment. ... work that forms part of this product is prohibited.</p>\r\n",
    "system_requirements": {
      "windows": {
        "minimum": {
          "os": "Windows XP SP3",
          "storage": "10 GB",
          "processor": "Intel Core 2 Duo / AMD 2.4GHz",
          "memory": "2 GB",
          "graphics": "512 MB VRAM",
          "sound_card": null,
          "directx": "9.0",
          "network": null
        },
        "recommended": {
          "os": "Windows 7",
          "storage": "10 GB",
          "processor": "Quad Core",
          "memory": "2 GB",
          "graphics": "1 GB VRAM",
          "sound_card": null,
          "directx": "11.0",
          "network": null
        }
      }
    },
    "system_requirements_notes": null,
    "supported_locales": [
      {
        "locale": "pt-BR",
        "interface": true,
        "audio": false,
        "subtitle": false
      },
      {
        "locale": "en",
        "interface": true,
        "audio": true,
        "subtitle": true
      },
      {
        "locale": "de",
        "interface": true,
        "audio": false,
        "subtitle": false
      },
      {
        "locale": "es",
        "interface": true,
        "audio": false,
        "subtitle": false
      },
      {
        "locale": "fr",
        "interface": true,
        "audio": false,
        "subtitle": false
      },
      {
        "locale": "it",
        "interface": true,
        "audio": false,
        "subtitle": false
      }
    ],
    "parental_ratings": [
      {
        "organization": "ClassInd",
        "classification": "Not recommended for people under 18",
        "content_descriptors": [
          "Extreme Violence",
          "Inappropriate Language"
        ]
      }
    ],
    "embeds": [
      {
        "url": "http://www.youtube.com/watch?v=BhutLDpjRDA",
        "thumbnail": "https://assets.nuuvem.com/image/upload/t_video_overlay_transformation/v1/products/embeds/thumbnails/yv2jtpew8gkrkufcpzgx.jpg",
        "code": "<iframe src='//www.youtube.com/embed/BhutLDpjRDA' frameborder='0' allow='autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>",
        "type": "video"
      }
    ],
    "release_date": "2014-02-25T03:00:00Z"
  },
  "relationships": {
    "categories": {
      "data": [
        {
          "id": "557ca3cf69702d235300003d",
          "type": "categories"
        },
        {
          "id": "557ca3cf69702d2353000041",
          "type": "categories"
        },
        {
          "id": "563cfdf669702d5edd000164",
          "type": "categories"
        }
      ]
    },
    "platforms": {
      "data": [
        {
          "id": "5e3c5fe6cb8db15d5aff3180",
          "type": "platforms"
        }
      ]
    },
    "systems": {
      "data": [
        {
          "id": "557ca3cf69702d2353000018",
          "type": "systems"
        }
      ]
    },
    "activations": {
      "data": [
        {
          "id": "557ca3cf69702d235300005a",
          "type": "activations"
        }
      ]
    },
    "developers": {
      "data": [
        {
          "id": "5a3970db88102444ef00021b",
          "type": "developers"
        }
      ]
    },
    "publishers": {
      "data": [
        {
          "id": "557ce4bf69702d0c6e1e0100",
          "type": "publishers"
        }
      ]
    },
    "product_type": {
      "data": {
        "id": "557ca3cf69702d235300000b",
        "type": "product_types"
      }
    }
  },
  "links": {
    "self": "https://api.nuuvem.com/v3/br/products/557dbc2c69702d0a9c36d900"
  },
  "included": [
    {
      "id": "557ca3cf69702d235300003d",
      "type": "categories",
      "attributes": {
        "name": "Action"
      },
      "links": {
        "self": "https://api.nuuvem.com/v3/categories/557ca3cf69702d235300003d"
      }
    },
    {
      "id": "557ca3cf69702d2353000041",
      "type": "categories",
      "attributes": {
        "name": "Adventure"
      },
      "links": {
        "self": "https://api.nuuvem.com/v3/categories/557ca3cf69702d2353000041"
      }
    },
    {
      "id": "563cfdf669702d5edd000164",
      "type": "categories",
      "attributes": {
        "name": "Hack and Slash"
      },
      "links": {
        "self": "https://api.nuuvem.com/v3/categories/563cfdf669702d5edd000164"
      }
    },
    {
      "id": "5e3c5fe6cb8db15d5aff3180",
      "type": "platforms",
      "attributes": {
        "name": "PC"
      },
      "links": {
        "self": "https://api.nuuvem.com/v3/platforms/5e3c5fe6cb8db15d5aff3180"
      }
    },
    {
      "id": "557ca3cf69702d2353000018",
      "type": "systems",
      "attributes": {
        "name": "Windows"
      },
      "links": {
        "self": "https://api.nuuvem.com/v3/systems/557ca3cf69702d2353000018"
      }
    },
    {
      "id": "557ca3cf69702d235300005a",
      "type": "activations",
      "attributes": {
        "name": "Steam"
      },
      "links": {
        "self": "https://api.nuuvem.host/v3/activations/557ca3cf69702d235300005a"
      }
    },
    {
      "id": "5a3970db88102444ef00021b",
      "type": "developers",
      "attributes": {
        "name": "MercurySteam"
      },
      "links": {
        "self": "https://api.nuuvem.com/v3/developers/5a3970db88102444ef00021b"
      }
    },
    {
      "id": "557ce4bf69702d0c6e1e0100",
      "type": "publishers",
      "attributes": {
        "name": "Konami Digital Entertainment"
      },
      "links": {
        "self": "https://api.nuuvem.com/v3/publishers/557ce4bf69702d0c6e1e0100"
      }
    },
    {
      "id": "557ca3cf69702d235300000b",
      "type": "product_types",
      "attributes": {
        "name": "Standard"
      },
      "links": {
        "self": "https://api.nuuvem.com/v3/product_types/557ca3cf69702d235300000b"
      }
    }
  ]
}
```

**ATTRIBUTES**

| Name                        | Description                                                                                                       |
| --------------------------- | ----------------------------------------------------------------------------------------------------------------- |
| `id`                        | String representing the product id                                                                                |
| `type`                      | Resource type                                                                                                     |
| `name`                      | Name of the product                                                                                               |
| `sku`                       | Integer value that represents the product                                                                         |
| `slug`                      | Represents the parameterized product name                                                                         |
| `store_url`                 | URL of the product on Nuuvem store                                                                                |
| `purchasable`               | Indicates whether the product is available for purchase or not for the given country code url param               |
| `pricing`                   | Contains detailed pricing information for a product, including currency, amounts, discounts, and validity periods |
| `images`                    | List of all product images                                                                                        |
| `about`                     | Description of the product as rich text                                                                           |
| `short_info`                | Short description of the product as rich text                                                                     |
| `important_information`     | Important information about the product as rich text                                                              |
| `pre_sale_information`      | Information about pre-sale of the product as rich text                                                            |
| `bonus_content`             | Bonus information about the product as rich text                                                                  |
| `highlights`                | Highlights about the product as rich text                                                                         |
| `footer_notes`              | Footer notes about the product as rich text                                                                       |
| `system_requirements`       | Hardware and software specifications required to play the game                                                    |
| `system_requirements_notes` | Requirements notes due to some game specificity                                                                   |
| `supported_locales`         | Languages supported by the game                                                                                   |
| `parental_ratings`          | Parental ratings recommended for the game, related to the supplied `country-code`                                 |
| `embeds`                    | List of embedded multimedia                                                                                       |
| `release_date`              | Game release date in the ISO 8601 format for the given country code or an approximate date. E.g.: "End of 2025"   |

**RELATIONSHIPS**

Represent all product system-related objects.

A relationship is represented by a key named after the related object, e.g., 'Categories', and each relationship has an array with key-value pairs of `id` and `type`.

**LINKS**

| Name   | Description                                                       |
| ------ | ----------------------------------------------------------------- |
| `self` | The API url for product resources provides access of product data |

**INCLUDED**

Returns a list of serialized objects from relationships. The list below contains all related product objects.

- [Categories](#categories)
- [Platforms](#platforms)
- [Systems](#systems)
- [Activations](#activations)
- [Product Types](#product-types)
- [Developers](#developers)
- [Publishers](#publishers)

## GET: List products

`GET /v3/:country-code/products`

> Get a list of products.

```shell
curl -i -X GET 'https://api.nuuvem.com/v3/br/products' \
  -H "Authorization: Bearer ${NUUVEM_API_TOKEN}" \
  -H "Accept: application/vnd.api+json" \
  -H "Accept-Language: en"
```

> The above command returns the following **response body and headers**:

```http
HTTP/2.0 200 OK
Content-Type: application/vnd.api+json; charset=utf-8
Content-Language: en

{
  "data": [
    {
      "id": "557dba1f69702d0a9cc70000",
      "type": "products",
      "attributes": {
        "name": "Kairo",
        // Same attributes as the product object details section
      }
    },
    {
      "id": "557dba2269702d0a9c250100",
      "type": "products",
      "attributes": {
        "name": "Manhunter",
        // Same attributes as the product object details section
      }
    }
  ],
  "meta": {
    "pagination": {
      "current": 2,
      "first": 1,
      "prev": 1,
      "next": 3,
      "last": 3080,
      "records": 6159
    }
  },
  "links": {
    "self": "https://api.nuuvem.com/v3/br/products?page%5Bsize%5D=2&page%5Bnumber%5D=2",
    "current": "https://api.nuuvem.com/v3/br/products?page[number]=2&page[size]=2",
    "first": "https://api.nuuvem.com/v3/br/products?page[number]=1&page[size]=2",
    "prev": "https://api.nuuvem.com/v3/br/products?page[number]=1&page[size]=2",
    "next": "https://api.nuuvem.com/v3/br/products?page[number]=3&page[size]=2",
    "last": "https://api.nuuvem.com/v3/br/products?page[number]=3080&page[size]=2"
  }
}
```

> **Note**: All fields will be returned regardless of whether they contain information or are null, where null represents the absence of information.

**URL PARAMETERS**

| Parameter      | Default | Description                                                                |
| -------------- | ------- | -------------------------------------------------------------------------- |
| `country-code` | none    | A valid ISO 3166-1 A-2 country code from [LIST COUNTRIES](#list-countries) |

**QUERY PARAMETERS**

| Parameter      | Default | Description                                                                   |
| -------------- | ------- | ----------------------------------------------------------------------------- |
| `page[size]`   | 20      | The number of products to retrieve per page. Maximum of 50 products per page. |
| `page[number]` | 1       | The page number you wish to retrieve.                                         |

[See noteworthy response attributes.](#product-object)

## GET: Retrieve product

`GET /v3/:country-code/products/:id`

> Get a product.

```shell
curl -i -X GET 'https://api.nuuvem.com/v3/br/products/557dbc2c69702d0a9c36d900' \
  -H "Authorization: Bearer ${NUUVEM_API_TOKEN}" \
  -H "Accept: application/vnd.api+json" \
  -H "Accept-Language: en"
```

> The above command returns the following **response body and headers**:

```http
HTTP/2.0 200 OK
Content-Type: application/vnd.api+json; charset=utf-8
Content-Language: en

{
  "data": {
    "id": "557dbc2c69702d0a9c36d900",
    "type": "products",
    "attributes": {
      "name": "Castlevania: Lords of Shadow 2",
      // Same attributes as the product object details section
    }
  }
}
```

> **Note**: All fields will be returned regardless of whether they contain information or are null, where null represents the absence of information.

**URL PARAMETERS**

| Parameter      | Default | Description                                                                |
| -------------- | ------- | -------------------------------------------------------------------------- |
| `country-code` | none    | A valid ISO 3166-1 A-2 country code from [LIST COUNTRIES](#list-countries) |
| `id`           | none    | The requested product ID                                                   |

[See noteworthy response attributes.](#product-object)
