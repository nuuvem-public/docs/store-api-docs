# Countries

The Store API requires the user to inform a valid and supported country in some endpoints as a URL path param. At the time of your request, you can call the [LIST COUNTRIES](#list-countries) endpoint to get the currently available and supported list of countries.

This section description provides information about the request method, headers, response status, content type, and body structure, helping users understand how to interact with the `/v3/countries` endpoint and interpret the response.

## Country object

Detailed **Country** resource attributes descriptions with payload example.

```json
{
  "id": "076",
  "type": "countries",
  "attributes": {
    "alpha2": "BR",
    "name": "Brazil"
  }
}
```

**ATTRIBUTES**

| Name     | Description                           |
| -------- | ------------------------------------- |
| `id`     | Numeric ISO 3166-1 code               |
| `type`   | Resource type                         |
| `alpha2` | Country code in ISO 3166-1 A-2 format |
| `name`   | Country name                          |

## GET: List Countries

> Get the list of countries:

```shell
curl -X GET 'https://api.nuuvem.com/v3/countries' \
  -H "Authorization: Bearer ${NUUVEM_API_TOKEN}" \
  -H "Accept: application/vnd.api+json" \
  -H "Accept-Language: en"
```

> The above command returns the following **response body and headers**:

```http
HTTP/2.0 200 OK
Content-Type: application/vnd.api+json; charset=utf-8
Content-Language: en

{
  "data": [
    {
      "id": "076",
      "type": "country",
      "attributes": {
        "alpha2": "BR",
        "name": "Brazil"
      }
    },
    ...
  ]
}

```

`GET /v3/countries`

| Parameter | Default | Description |
| --------- | ------- | ----------- |

You don't have to supply any parameters for this endpoint.

[See noteworthy response attributes.](#country-object)
