# Product Types

Product type identifies the game's version, such as standard, DLC, premium editions, or others.

## Product Type object

Detailed descriptions of product type resource attributes with example payload.

```json
{
  "id": "557ca3cf69702d235300000b",
  "type": "product_types",
  "attributes": {
    "name": "Standard"
  }
}
```

**ATTRIBUTES**

| Name   | Description                                         |
| ------ | --------------------------------------------------- |
| `id`   | String representing the product type id             |
| `type` | Resource type                                       |
| `name` | Localized string representing the product type name |

## GET: Retrieve Product Type

`GET /v3/product_types/:id`

> Get a product type.

```shell
curl -i -X GET 'https://api.nuuvem.com/v3/product_types/557ca3cf69702d235300000b' \
  -H "Authorization: Bearer ${NUUVEM_API_TOKEN}" \
  -H "Accept: application/vnd.api+json" \
  -H "Accept-Language: en"
```

> The above command returns the following **response body and headers**:

```http
HTTP/2.0 200 OK
Content-Type: application/vnd.api+json; charset=utf-8
Content-Language: en

{
  "id": "557ca3cf69702d235300000b",
  "type": "product_types",
  "attributes": {
    "name": "Standard"
  },
  "links": {
    "self": "https://api.nuuvem.com/v3/product_types/557ca3cf69702d235300000b"
  }
}
```

| Parameter | Default | Description                   |
| --------- | ------- | ----------------------------- |
| `id`      | none    | The requested product type ID |

**LINKS**

| Name   | Description                                           |
| ------ | ----------------------------------------------------- |
| `self` | URL for the selected product type resource on the API |
