# Platforms

Video game platforms, such as PC, Xbox, PlayStation, Nintendo Switch and Mobile, determine the devices a game is compatible with.

## Platform object

Detailed Platform resource attributes descriptions with payload example.

```json
{
  "id": "5e3c5fe6cb8db15d5aff3180",
  "type": "platforms",
  "attributes": {
    "name": "PC"
  }
}
```

**ATTRIBUTES**

| Name   | Description                                     |
| ------ | ----------------------------------------------- |
| `id`   | String representing the platform id             |
| `type` | Resource type                                   |
| `name` | Localized string representing the platform name |

## GET: Retrieve platform

`GET /v3/platforms/:id`

> Get a platform.

```shell
curl -i -X GET 'https://api.nuuvem.com/v3/platforms/5e3c5fe6cb8db15d5aff3180' \
  -H "Authorization: Bearer ${NUUVEM_API_TOKEN}" \
  -H "Accept: application/vnd.api+json" \
  -H "Accept-Language: en"
```

> The above command returns the following **response body and headers**:

```http
HTTP/2.0 200 OK
Content-Type: application/vnd.api+json; charset=utf-8
Content-Language: en

{
  "data": {
    "id": "5e3c5fe6cb8db15d5aff3180",
    "type": "platforms",
    "attributes": {
      "name": "PC"
    },
    "links": {
      "self": "https://api.nuuvem.com/v3/platforms/5e3c5fe6cb8db15d5aff3180"
    }
  }
}
```

| Parameter | Default | Description               |
| --------- | ------- | ------------------------- |
| `id`      | none    | The requested platform ID |

**LINKS**

| Name   | Description                                       |
| ------ | ------------------------------------------------- |
| `self` | URL for the selected platform resource on the API |
