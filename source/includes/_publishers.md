# Publishers

Represents companies responsible for game publishing and distribution, such as Electronic Arts (EA), Activision, Ubisoft, and Nintendo.

## Publisher object

Detailed descriptions of publisher resource attributes with example payload.

```json
{
  "id": "557ce4bf69702d0c6e1e0100",
  "type": "publishers",
  "attributes": {
    "name": "Konami Digital Entertainment"
  }
}
```

**ATTRIBUTES**

| Name   | Description                            |
| ------ | -------------------------------------- |
| `id`   | String representing the publisher id   |
| `type` | Resource type                          |
| `name` | String representing the publisher name |

## GET: Retrieve Publisher

`GET /v3/publishers/:id`

> Get a publisher.

```shell
curl -i -X GET 'https://api.nuuvem.com/v3/publishers/557ce4bf69702d0c6e1e0100' \
  -H "Authorization: Bearer ${NUUVEM_API_TOKEN}" \
  -H "Accept: application/vnd.api+json" \
  -H "Accept-Language: en"
```

> The above command returns the following **response body and headers**:

```http
HTTP/2.0 200 OK
Content-Type: application/vnd.api+json; charset=utf-8
Content-Language: en

{
  "id": "557ce4bf69702d0c6e1e0100",
  "type": "publishers",
  "attributes": {
    "name": "Konami Digital Entertainment"
  },
  "links": {
    "self": "https://api.nuuvem.com/v3/publishers/557ce4bf69702d0c6e1e0100"
  }
}
```

| Parameter | Default | Description                 |
| --------- | ------- | --------------------------- |
| `id`      | none    | The requested publisher ID |

**LINKS**

| Name   | Description                                         |
| ------ | --------------------------------------------------- |
| `self` | URL for the selected publisher resource on the API |
