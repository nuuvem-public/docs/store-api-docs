# Categories

Categories refer to the genre (type) of video game. This includes categories like action, adventure, first-person shooter (FPS), and many more.

## Category object

Detailed Category resource attributes descriptions with payload example.

```json
{
  "id": "557ca3cf69702d235300003d",
  "type": "categories",
  "attributes": {
    "name": "Action"
  }
}
```

**ATTRIBUTES**

| Name   | Description                                     |
| ------ | ----------------------------------------------- |
| `id`   | String representing the category id             |
| `type` | Resource type                                   |
| `name` | Localized string representing the category name |

## GET: Retrieve category

`GET /v3/categories/:id`

> Get a category.

```shell
curl -i -X GET 'https://api.nuuvem.com/v3/categories/557ca3cf69702d235300003d' \
  -H "Authorization: Bearer ${NUUVEM_API_TOKEN}" \
  -H "Accept: application/vnd.api+json" \
  -H "Accept-Language: en"
```

> The above command returns the following **response body and headers**:

```http
HTTP/2.0 200 OK
Content-Type: application/vnd.api+json; charset=utf-8
Content-Language: en

{
  "data": {
    "id": "557ca3cf69702d235300003d",
    "type": "categories",
    "attributes": {
      "name": "Action"
    },
    "links": {
      "self": "https://api.nuuvem.com/v3/categories/557ca3cf69702d235300003d"
    }
  }
}
```

| Parameter | Default | Description               |
| --------- | ------- | ------------------------- |
| `id`      | none    | The requested category ID |

**LINKS**

| Name   | Description                                                              |
| ------ | ------------------------------------------------------------------------ |
| `self` | The API url for category resources provides access of game category data |
