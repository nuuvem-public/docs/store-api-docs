# Systems

Operating systems the game is supported on, like Windows, macOS, and others.

## System object

Detailed descriptions of system resource attributes with example payloads.

```json
{
  "id": "557ca3cf69702d2353000018",
  "type": "systems",
  "attributes": {
    "name": "Windows"
  }
}
```

**ATTRIBUTES**

| Name   | Description                         |
| ------ | ----------------------------------- |
| `id`   | String representing the system id   |
| `type` | Resource type                       |
| `name` | String representing the system name |

## GET: Retrieve system

`GET /v3/systems/:id`

> Get a system.

```shell
curl -i -X GET 'https://api.nuuvem.com/v3/systems/557ca3cf69702d2353000018' \
  -H "Authorization: Bearer ${NUUVEM_API_TOKEN}" \
  -H "Accept: application/vnd.api+json" \
  -H "Accept-Language: en"
```

> The above command returns the following **response body and headers**:

```http
HTTP/2.0 200 OK
Content-Type: application/vnd.api+json; charset=utf-8
Content-Language: en

{
  "data": {
    "id": "557ca3cf69702d2353000018",
    "type": "systems",
    "attributes": {
      "name": "Windows"
    },
    "links": {
      "self": "https://api.nuuvem.com/v3/systems/557ca3cf69702d2353000018"
    }
  }
}
```

| Parameter | Default | Description             |
| --------- | ------- | ----------------------- |
| `id`      | none    | The requested system ID |

**LINKS**

| Name   | Description                                     |
| ------ | ----------------------------------------------- |
| `self` | URL for the selected system resource on the API |
