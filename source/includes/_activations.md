# Activations

Platforms where you can activate the game, such as Steam, Epic, and others.

## Activation object

Detailed descriptions of platform activation resource attributes with example payload.

```json
{
  "id": "557ca3cf69702d235300005a",
  "type": "activations",
  "attributes": {
    "name": "Steam"
  }
}
```

**ATTRIBUTES**

| Name   | Description                             |
| ------ | --------------------------------------- |
| `id`   | String representing the activation id   |
| `type` | Resource type                           |
| `name` | String representing the activation name |

## GET: Retrieve activation

`GET /v3/activations/:id`

> Get an activation.

```shell
curl -i -X GET 'https://api.nuuvem.com/v3/activations/557ca3cf69702d235300005a' \
  -H "Authorization: Bearer ${NUUVEM_API_TOKEN}" \
  -H "Accept: application/vnd.api+json" \
  -H "Accept-Language: en"
```

> The above command returns the following **response body and headers**:

```http
HTTP/2.0 200 OK
Content-Type: application/vnd.api+json; charset=utf-8
Content-Language: en

{
  "data": {
    "id": "557ca3cf69702d235300005a",
    "type": "activations",
    "attributes": {
      "name": "Windows"
    },
    "links": {
      "self": "https://api.nuuvem.com/v3/activations/557ca3cf69702d235300005a"
    }
  }
}
```

| Parameter | Default | Description                 |
| --------- | ------- | --------------------------- |
| `id`      | none    | The requested activation ID |

**LINKS**

| Name   | Description                                         |
| ------ | --------------------------------------------------- |
| `self` | URL for the selected activation resource on the API |
