---
title: API Reference

language_tabs: # must be one of https://github.com/rouge-ruby/rouge/wiki/List-of-supported-languages-and-lexers
  - shell

toc_footers:
  - <a href='https://github.com/slatedocs/slate'>Documentation Powered by Slate</a>

includes:
  - introduction
  - countries
  - products
  - categories
  - platforms
  - systems
  - activations
  - product_types
  - developers
  - publishers

search: true

code_clipboard: true

meta:
  - name: description
    content: Documentation for the Nuuvem Store API
---
