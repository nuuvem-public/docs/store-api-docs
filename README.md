# Nuuvem Store API Docs

This repository is responsible for containing the code for generating the Nuuvem Store API documentation. Read below for more information about executing, creating, editing, building and deploying.

## Requirements

It is required that the `docker` installed in order for the project to be successfully set up and used.

## Installation

To install the docs, simply follow these steps:

- Clone the project with:
  ```sh
  $ git clone git@gitlab.com:nuuvem-public/docs/store-api-docs.git
  ```
- Navigate to the project folder:
  ```sh
  $ cd store-api-docs
  ```

## Running the application

To run the application, simply follow these steps:

  - Run the server:
    ```sh
    $ bin/server
    ```

## Deploy

The generated static HTML files are built automatically by GitLab CI (see `.gitlab-ci.yml`) on each commit to the `main` branch pushed to this repository, and are made publicly available at <https://nuuvem-public.gitlab.io/docs/store-api-docs/> and <https://docs.nuuvem.com/> via [GitLab Pages](https://gitlab.com/nuuvem-public/docs/store-api-docs/pages).

## Getting Started with Slate
------------------------------

To get started with Slate, please check out the [Getting Started](https://github.com/slatedocs/slate/wiki#getting-started)
section in their [wiki](https://github.com/slatedocs/slate/wiki).

You can view more in [the list on the wiki](https://github.com/slatedocs/slate/wiki/Slate-in-the-Wild).
